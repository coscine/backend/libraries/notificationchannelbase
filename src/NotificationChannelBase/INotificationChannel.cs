﻿using Coscine.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coscine.NotificationChannelBase
{
    public interface INotificationChannel
    {
        Task SendAsync(IConfiguration c, Message m);
    }
}
