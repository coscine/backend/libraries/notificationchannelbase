﻿using Coscine.Database.DataModel;
using Coscine.Database.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coscine.NotificationChannelBase
{
    public class Message
    {
        public JObject MessageData { get; }
        public JObject RequestBody { get; }
        public User User { get;  }
        public Project Project { get; } = null;

        public Message (JObject messageData, JObject requestBody, User user, Project project = null)
        {
            if (messageData == null || requestBody == null || user == null)
            {
                throw new ArgumentException("Parameter missing for Message creation.");
            }
            MessageData = messageData;
            RequestBody = requestBody;
            User = user;
            Project = project;
        }
    }
}
