﻿using Coscine.NotificationChannelBase;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Claims;

namespace NotificationChannelBase.Test
{
    [TestFixture]
    public class MessageTest
    {
        public MessageTest()
        {
        }

        [OneTimeSetUp]
        public void Setup()
        {
            
        }

        [OneTimeTearDown]
        public void End()
        {
        }

        [Test]
        public void TestMessageWithNullValues()
        {
            
            Message message = null;
            Assert.Throws<ArgumentException>(() => message = new Message(null, null, null));
        }
    }
}
